//https://felipelinsmachado.com/eslint-react/
module.exports = {
    env: {
      browser: true,
      commonjs: true,
      es6: true
    },
    extends: ['eslint:recommended', 'prettier', 'react-app'],
    plugins: ['prettier', 'react'],
    parserOptions: {
      ecmaFeatures: {
        experimentalObjectRestSpread: true,
        jsx: true
      },
      sourceType: 'module'
    },
    rules: {
      'no-console': 1,
      quotes: [1, 'single'],
      semi: [2, 'always'],
      //'prettier/prettier': [1, {singleQuote: true, trailingComma: 'none'}]
    }
  };
  