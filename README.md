## React-Redux/Saga Form demo

### Provides
- react ^16.6.0
- redux ^4.0.1
- redux-saga ^0.16.2

### Installation

- `yarn install`
- `yarn start`
- `yarn test`
- `yarn build`

## Folder Structure

After creation, your project should look like this:

```
from-demo/
  README.md
  node_modules/
  package.json
  public/
    index.html
    favicon.ico
  src/
    actions
    api
    config-store 
    components   
    containers
    reducer
    sagas
    utilities
    App.scss
    App.test.js
    index.js
```
