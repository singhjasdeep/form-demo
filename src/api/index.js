import axios from 'axios';

const config = {
    headers: { accept: 'application/json' }
  };

  export const get = async(url) => {
    const response = await axios
                        .get(url, config)
                        .then(function(response) { 
                        return ({ status: response.status, data: response });
                        })
                        .catch(function(error) {
                        if (error && error.response) {
                            return (error.response);
                        } else {
                            return (error);
                        }
                    }); 
                return response;    
  };