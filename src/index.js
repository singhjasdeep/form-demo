import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import 'bootstrap/dist/css/bootstrap.css';
import { store } from './config-store';
import Form from './containers/Form';

ReactDOM.render(
    <Provider store={store}>
        <Form />
    </Provider>, 
  document.getElementById('root')
);


