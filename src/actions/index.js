/*
 * @file: index.js
 * @description: All redux actions.
 * */
export const FETCH_FORM_FIELDS = 'FETCH_FORM_FIELDS';

export const fetch_form_fields = (data) => {
  return {
    type: FETCH_FORM_FIELDS,
    payload:data
  };
};

