/*
 * @file: Regex.js
 * @description: Regex used for validation in application.
 * */

var Regex = {
    validateEmail: val => {
      // eslint-disable-next-line
      return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(
        val
      );
    },
    validateMobile: val => {
      return /^\+?\d{9,12}$/.test(val);
    },
  };
  
  export default Regex;
  