/*
 * @file: saga-actions.js
 * @description: All saga actions.
 * */

import { call, put } from 'redux-saga/effects';
import {fetch_form_fields} from '../actions';
import * as SagaTypes from './saga-types';
//import * as Api from '../api';

export function* fetchFormsSaga() {
   try {
     /*
      'https://ansible-template-engine.herokuapp.com/form'
       Api returning origin access error for localhost
     */
      const data = [
                    {"label":"Email address","type":"email","isOptional":false,"isHidden":false},
                    {"label":"Gender","type":"radio","value":["M (Male)","F (Female)","X (Indeterminate/Intersex/Unspecified)"],"isOptional":true},
                    {"label":"State","type":"select","value":["NSW","QLD","SA","TAS","VIC","WA","NT","ACT"],"default":"ACT"},
                    {"label":"Contact number","type":"telephone"},
                    {"type":"hidden","value":1540672352359,"isHidden":true}
                  ];//yield call(Api.get,'https://ansible-template-engine.herokuapp.com/form');
      
      if(data){
        yield put(fetch_form_fields(data));
      }
      
   } catch (e) {
    console.warn(e);
   }
}

export const fetchForms = () => ({
  type: SagaTypes.FETCH_FORMS
});



