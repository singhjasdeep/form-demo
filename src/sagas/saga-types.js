/*
 * @file: saga-types.js
 * @description: All saga types.
 * */
export const FETCH_FORMS = 'FETCH_FORMS';