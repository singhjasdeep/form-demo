/*
 * @file: index.js
 * @description: Saga function setup.
 * */

import { all,takeLatest } from 'redux-saga/effects';
import * as SAGA from './saga-actions';
import * as SagaTypes from './saga-types';

/************** Bind all saga function here ***********/

export default function* sagaFun (){
  yield all([
    takeLatest(SagaTypes.FETCH_FORMS, SAGA.fetchFormsSaga)
  ]);
};