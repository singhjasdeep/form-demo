/*********** Redux-Saga ************/
import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware from 'redux-saga';
import sagas from '../sagas';
import reducer from '../reducer';

// create the saga middleware
const sagaMiddleware = createSagaMiddleware();
// mount it on the Store
export const store = createStore(
  reducer,
  composeWithDevTocomposeols(applyMiddleware(sagaMiddleware))
);
// then run the saga
sagaMiddleware.run(sagas);