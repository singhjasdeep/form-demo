import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Form } from 'reactstrap';
import { fetchForms } from '../sagas/saga-actions';
import Select from '../components/select-box';
import RadioButton from '../components/radio-button';
import Input from '../components/input-box';
import Button from '../components/button';
import Regex from '../utilities/regex';
import '../App.scss';

class AppForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      formData:[]
    };
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount(){
    this.props.fetchForms();
  }

  handleSubmit(event){
    event.preventDefault();
    const fields = this.props.forms.simpleForm.map( field => {
        let isValid = true;
        if(field.hasOwnProperty('isOptional') && !field.isOptional && !event.target[field.type].value){
          isValid = false;
        } else if(event.target[field.type].value && field.type === 'email'){
          isValid = Regex.validateEmail(event.target[field.type].value);
        } else if(event.target[field.type].value && field.type === 'telephone'){
          isValid = Regex.validateMobile(event.target[field.type].value);
        }        
        return({
            label: field.label, 
            value: event.target[field.type].value,
            isValid
          });
    });
    this.setState({formData:fields});
  }

  formFields(fields){  
    return fields.map((field, index) => {
            switch (field.type) {
              case 'select':
                return <Select field={field} key={index}/>;
              case 'radio':
                return <RadioButton field={field} key={index}/>;
              default:
                return <Input field={field} key={index} />;  
            }
      });
  };

  render() {
    const {forms} = this.props;
    const {formData} = this.state;
    return (
        <section id="main-content">
          <div className="wrapper">
            <h3>Basic Form</h3><br/>
            <Form onSubmit={this.handleSubmit}>
                {this.formFields(forms.simpleForm)}
                <Button title="Submit"/>              
            </Form>
            <br/><br/>
            <h5>Ouput:- <br/>
              {formData.length > 0 && JSON.stringify(formData)}
            </h5>
          </div>
        </section>
    );
  }
}

AppForm.propTypes = {
  forms: PropTypes.object.isRequired,
  fetchForms: PropTypes.func.isRequired
};

const mapStateToProps = state => ({
  forms: state.forms
});

const mapDispatchToProps = dispatch => ({
  fetchForms: bindActionCreators(fetchForms, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(AppForm);
