import React from 'react';
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';
import sagaMiddleware from 'redux-saga';
import Adapter from 'enzyme-adapter-react-15';
import { render, configure } from 'enzyme';
import Form from './containers/Form';
import Button from './components/button';
import InputBox from './components/input-box';

//action 
import * as Action from './actions';
//reducer 
import forms from './reducer/forms';

configure({ adapter: new Adapter() });
const middlewares = [sagaMiddleware];
const mockStore = configureMockStore(middlewares);

describe('Components', () => {
  it('renders form component without crashing', () => {
    const store = mockStore({
      forms: {
        simpleForm: []
      }
    });
    // eslint-disable-next-line
    const component = render(
      <Provider store={store}>
        <Form />
      </Provider>
    );
    expect(component).toMatchSnapshot();
  });

  it('renders button component without crashing', () => {
    // eslint-disable-next-line
    const component = render(
        <Button title="Submit"/>
    );
    expect(component).toMatchSnapshot();
  });

  it('renders button component without crashing', () => {
    // eslint-disable-next-line
    const field = {
      label: "First Name",
      type: "text",
      value: ""
    };
    const component = render(
        <InputBox field={field} key={1}/>
    );
    expect(component).toMatchSnapshot();
  });

});

describe('actions', () => {
  it('should create an action to fetch form fields', () => {
    const data = [
      {"label":"Email address","type":"email","isOptional":false,"isHidden":false},
      {"label":"Gender","type":"radio","value":["M (Male)","F (Female)","X (Indeterminate/Intersex/Unspecified)"],"isOptional":true},
      {"label":"State","type":"select","value":["NSW","QLD","SA","TAS","VIC","WA","NT","ACT"],"default":"ACT"},
      {"label":"Contact number","type":"telephone"},
      {"type":"hidden","value":1540672352359,"isHidden":true}
    ];
    const expectedAction = {
      type: "FETCH_FORM_FIELDS",
      payload: data
    }
    expect(Action.fetch_form_fields(data)).toEqual(expectedAction)
  })
})

describe('Form reducer', () => {
  it('should return the form reducer', () => {
    const initialState = {
      simpleForm: []
    };
    const data = [
      {"label":"Email address","type":"email","isOptional":false,"isHidden":false},
      {"label":"Gender","type":"radio","value":["M (Male)","F (Female)","X (Indeterminate/Intersex/Unspecified)"],"isOptional":true},
      {"label":"State","type":"select","value":["NSW","QLD","SA","TAS","VIC","WA","NT","ACT"],"default":"ACT"},
      {"label":"Contact number","type":"telephone"},
      {"type":"hidden","value":1540672352359,"isHidden":true}
    ];
    expect(forms(initialState, Action.fetch_form_fields(data))).toEqual({...initialState, simpleForm: data })
  })
})
