/*
 * @file: index.js
 * @description: All combine reducers.
 * */

import { combineReducers } from 'redux';
import forms from './forms';

const reducer = combineReducers({
   forms
});

export default reducer;