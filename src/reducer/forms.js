/*
 * @file: forms.js
 * @description: Forms reducer.
 * */

import * as Action from '../actions';

const initialState = {
    simpleForm: []
};

const forms = (state = initialState, action) => {
    switch (action.type) {
        case Action.FETCH_FORM_FIELDS:
            return {...state, simpleForm: action.payload};
        default:
            return state;
    }
};

export default forms;