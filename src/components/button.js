import React from 'react';
import PropTypes from 'prop-types';
import { Col, FormGroup, Button } from 'reactstrap';

const button = ({title}) => {
    return(
            <FormGroup check row>
                <Col sm={{ size: 10, offset: 2 }}>
                <Button type="submit">{title}</Button>
                </Col>
            </FormGroup>
    );
};

button.propTypes = {
    title: PropTypes.string.isRequired
};

export default button;