import React from 'react';
import PropTypes from 'prop-types';
import { Col, FormGroup, Label, Input } from 'reactstrap';

const selectBox = ({field, key}) => {

    const renderOptions = (options) => {
        return options.map((option, index) => 
            <option value={option} key={`option-${index}`}>{option}</option> 
        );
    };

    return(
        <FormGroup row key={key}>
            <Label htmlFor={field.label} sm={2}>{field.label}</Label>
            <Col sm={10}>
            <Input type="select" name="select" defaultValue={field.default}>
                {renderOptions(field.value)}
            </Input>
            </Col>
        </FormGroup>
    );
};

selectBox.propTypes = {
    field: PropTypes.object.isRequired,
    key: PropTypes.number.isRequired
};

export default selectBox;