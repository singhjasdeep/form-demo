import React from 'react';
import PropTypes from 'prop-types';
import { Col, FormGroup, Label, Input } from 'reactstrap';

const radioButton = ({field, key}) => {

    const renderRadio = (group) => {
        return group.map((radio, index) =>
          <FormGroup check key={`radio-${index}`}>
            <Label check>
              <Input type="radio" name="radio"/>
               {radio}
            </Label>
          </FormGroup>
        );
    };

    return(
        <FormGroup tag="fieldset" row key={key}>
            <legend className="col-form-label col-sm-2">{field.label}</legend>
            <Col sm={10}>
            {renderRadio(field.value)}
            </Col>
        </FormGroup>
    );
};

radioButton.propTypes = {
    field: PropTypes.object.isRequired,
    key: PropTypes.number.isRequired
};

export default radioButton;