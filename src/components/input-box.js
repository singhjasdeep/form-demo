import React from 'react';
import PropTypes from 'prop-types';
import { Col, FormGroup, Input, Label } from 'reactstrap';

const inputBox = ({field, key}) => {
    return(
        <FormGroup row key={key}>
            <Label htmlFor={field.label} sm={2}>{field.label}</Label>
            <Col sm={10}>
            <Input 
                type={field.type} 
                name={field.type} 
                defaultValue={field.value || ''} 
                placeholder={field.label} 
            />
            </Col>
        </FormGroup>
    );
};

inputBox.propTypes = {
    field: PropTypes.object.isRequired,
    key: PropTypes.number.isRequired
};

export default inputBox;